import asyncio
import httpx
from twikit import Client

USERNAME = '@...'
EMAIL = '...'
PASSWORD = '...'

proxy_list = [
    'http://201.246.201.157:999',
    'http://147.139.196.244:8080',
    'http://45.181.123.129:999',
    'http://186.96.50.113:999',
    # .....
]
proxy = 'http://186.96.50.113:999'
proxies={
    'http://': httpx.HTTPTransport(proxy=proxy),
    'https://': httpx.HTTPTransport(proxy=proxy)
}
proxy_couner = 0
# Initialize client
client = Client(language='en-US')
cookies = {
    'guest_id_marketing': '',
    'guest_id_ads': '',
    'personalization_id': '',
    'guest_id': '',
    'att': '',
    'kdt': '',
    'twid': '',
    'ct0': '',
    'auth_token': ''
}

# Login to the service with provided user credentials
def login():
    # client.login(
    #     auth_info_1=USERNAME,
    #     auth_info_2=EMAIL,
    #     password=PASSWORD
    # )
    loginClient = Client(language='en-US')
    loginClient.login(
        auth_info_1=USERNAME,
        auth_info_2=EMAIL,
        password=PASSWORD
    )
    client.set_cookies(loginClient.get_cookies())

def updateClient():
    global client
    global proxy_couner
    global proxies
    proxy_index = proxy_couner % len(proxy_list)
    proxy_couner += 1
    proxies={
        'http://': httpx.HTTPTransport(proxy=proxy_list[proxy_index]),
        'https://': httpx.HTTPTransport(proxy=proxy_list[proxy_index])
    }
    client = Client(language='en-US', proxies=proxies)
    login()

save_tweet = []
api_count = 0
user_list = [
    '@elonmusk',
    # '@einzte',
    # '@elon_alerts',
    # '@AndreCronjeTech',
    # '@justinsuntron',
    # '@dahongfei',
    # '@alex_dreyfus',
    # '@bantg',
    # '@gavofyork',
    # '@bgarlinghouse',
    # '@SergeyNazarov',
    # '@RyoshiResearch',
    # '@sasha35625',
    # '@9x9x9eth',
    # '@AntonioMJuliano',
    # '@StaniKulechov',
    # '@0xferg',
    # '@jaredgrey',
    # '@WillemsTRA',
    # '@BigBeardSamurai',
    # '@shahafbg',
    # '@spark_ren',
    # '@WatanabeSota',
    # '@haydenzadams',
    # '@Sonnenshein',
    # '@Grayscale',
    # '@DCGco',
    # '@cz_binance',
    # '@SBF_FTX',
    # '@AlamedaTrabucco',
    # '@BNBCHAIN',
    # '@carolinecapital',
    # '@_RichardTeng',
    # '@Web3WithBinance',
    # '@BluzelleHQ',
    # '@renprotocol',
    # '@AttentionToken'
]

def get_user_id(username):
    user = client.search_user(username)
    if user:
        return user[0].id
    else:
        return None

def get_user_tweets(user_id):
    global proxy
    tweets = client.get_user_tweets(user_id, 'Tweets', count=1)
    return tweets

async def check_and_display_new_tweets(user):
    global client
    global proxies
    global api_count
    print('=================')
    user_id = get_user_id(user)
    print(user_id)
    while True:
        if user_id:
            user_tweets = get_user_tweets(user_id)
            if user_tweets:
                tweet = user_tweets[0]
                if tweet.id not in save_tweet:
                    save_tweet.append(tweet.id)
                    print(tweet.text)
                else:
                    print('Tweets are already in the array.')
            else:
                print('There are no new tweets from users.')
        else:
            print('User ID not found.')
        api_count = api_count + 1
        if api_count % 20 == 0:
            updateClient()
        await asyncio.sleep(2)

async def main():
    login()
    loops = [check_and_display_new_tweets(user) for user in user_list]
    await asyncio.gather(*loops)

asyncio.run(main())